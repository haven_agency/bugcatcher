<?php

return [
    'name' => env('APP_NAME'),
    'environments' => ['production', 'staging'],
    'elastic_search' => [
        'host'   => env('ES_HOST'),
        'key'    => env('ES_KEY'),
        'secret' => env('ES_SECRET')
    ]
];
