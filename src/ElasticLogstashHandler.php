<?php namespace Haven\BugCatcher;

use Elasticsearch\Client;

class ElasticLogstashHandler
{
    public function __construct(Client $client, array $options = array())
    {
        $this->client = $client;

        $this->options = array_merge(
            array(
                'index'          => 'exceptions-'.date('Y.m.d'),      // Elastic index name
                'type'           => 'logs',       // Elastic document type
                'ignore_error'   => false,          // Suppress exceptions
            ),
            $options
        );
    }

    public function write($record)
    {
        try {
            $this->client->index(
                [
                    'index' => $this->options['index'],
                    'type' => $this->options['type'],
                    'timeout' => '50ms',
                    'body' => json_decode($record, true)
                ]
            );
        } catch (\Exception $e) {
            // Well that didn't pan out...
            if (!$this->options['ignore_error']) {
                throw $e;
            }
        }
    }
}
