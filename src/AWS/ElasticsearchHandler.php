<?php namespace Haven\BugCatcher\AWS;

use Aws\Signature\SignatureV4;
use Aws\Credentials\Credentials;

use Elasticsearch\ClientBuilder;

class ElasticsearchHandler
{
    public function __construct($host = null, $key = null, $secret = null)
    {
        $this->host = $host ?? config('bugcatcher.elastic_search.host');
        $this->key = $key ?? config('bugcatcher.elastic_search.key');
        $this->secret = $secret ?? config('bugcatcher.elastic_search.secret');
    }

    public function getClient()
    {
        $credentials = new Credentials($this->key, $this->secret);

        $signature = new SignatureV4('es', 'us-west-2');

        $middleware = new SignatureMiddleware($credentials, $signature);
        $defaultHandler = ClientBuilder::defaultHandler();
        $awsHandler = $middleware($defaultHandler);

        $clientBuilder = ClientBuilder::create();
        $clientBuilder
            ->allowBadJSONSerialization()
            ->setHandler($awsHandler)
            ->setHosts([$this->host]);

        return $clientBuilder->build();
    }
}
