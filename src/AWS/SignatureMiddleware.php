<?php namespace Haven\BugCatcher\AWS;

use Aws\Credentials\CredentialsInterface;
use Aws\Signature\SignatureInterface;
use GuzzleHttp\Psr7\Request;

class SignatureMiddleware
{
    /**
     * @var \Aws\Credentials\CredentialsInterface
     */
    protected $credentials;

    /**
     * @var \Aws\Signature\SignatureInterface
     */
    protected $signature;

    /**
     * @param CredentialsInterface $credentials
     * @param SignatureInterface $signature
     */
    public function __construct(CredentialsInterface $credentials, SignatureInterface $signature)
    {
        $this->credentials = $credentials;
        $this->signature = $signature;
    }

    /**
     * @param $handler
     * @return callable
     */
    public function __invoke($handler)
    {
        return function ($request) use ($handler) {
            $headers = $request['headers'];
            if ($headers['Host']) {
                if (is_array($headers['Host'])) {
                    $headers['Host'] = array_map([$this, 'removePort'], $headers['Host']);
                } else {
                    $headers['Host'] = $this->removePort($headers['Host']);
                }
            }
            $psrRequest = new Request($request['http_method'], $request['uri'], $headers, $request['body']);
            $psrRequest = $this->signature->signRequest($psrRequest, $this->credentials);

            $request['headers'] = array_merge($psrRequest->getHeaders(), $request['headers']);

            return $handler($request);
        };
    }

    /**
     * AWS api seems to doesn't use port part in host field
     * @param string $host
     * @return string
     */
    protected function removePort($host)
    {
        return parse_url($host)['host'];
    }
}
