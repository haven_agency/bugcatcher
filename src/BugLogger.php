<?php namespace Haven\BugCatcher;

use Monolog\Formatter\LogstashFormatter;

class BugLogger extends AbstractLogger
{
    protected $logstashFormatter;

    protected $client;

    public function __construct(LogstashFormatter $logstashFormatter, ElasticLogstashHandler $client)
    {
        $this->logstashFormatter = $logstashFormatter;

        $this->client = $client;
    }

    public function log($levelName, $message, array $context = [])
    {
        $formatted = $this->getFormattedMessage($levelName, $message, $context);

        if (app()->environment(config('bugcatcher.environments'))) {
            $this->client->write($formatted);
        }
    }

    private function getFormattedMessage($levelName, $message, $context)
    {
        $data = [
            'level_name'  => $levelName,
            'message'     => $message,
            'context'     => $context,
            'channel'     => app()->environment(),
            'extra'       => $this->getExtraFields()
        ];

        return $this->logstashFormatter->format($data);
    }

    private function getExtraFields()
    {
        $rawPost = json_decode(file_get_contents('php://input'), true) ?: [];

        return [
            'REQUEST_PAYLOAD' => json_encode(array_merge($_REQUEST, $rawPost)),
            'REQUEST_URI'     => isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null,
            'REQUEST_METHOD'  => isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null,
            'HTTP_USER_AGENT' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null
        ];
    }
}
