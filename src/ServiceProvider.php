<?php

namespace Haven\BugCatcher;

use Monolog\Logger;
use Monolog\Handler\PsrHandler;
use Monolog\Formatter\LogstashFormatter;

use Illuminate\Contracts\Container\Container;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/bugcatcher.php' => config_path('bugcatcher.php')
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('bugcatcher', function () {
            $client = (new AWS\ElasticsearchHandler())->getClient();

            $logstash = new ElasticLogstashHandler($client);

            $formatter = new LogstashFormatter(config('bugcatcher.name'), null, null, '', 1);

            return new BugLogger($formatter, $logstash);
        });

        $this->app['log']->extend('bugcatcher', function (Container $app) {
            $handler = new PsrHandler($app['bugcatcher']);

            return new Logger('bugcatcher', [$handler]);
        });
    }
}
