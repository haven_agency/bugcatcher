# Changelog

All Notable changes to `BugCatcher` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## NEXT - 2018-03-12

### Added
- log app environment

### Fix
- Upgrade to Laravel 5.6

## NEXT - 2017-07-21

### Added
- Log Request Payload, User Agent, Request Method, URI




## NEXT - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

