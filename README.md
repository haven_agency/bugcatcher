# Laravel BugCatcher

BugCatcher collects app exceptions and stores them in elastic search.

## Kibana (Log Interface)

http://search-haven-elasticsearch-kdegklpm2od5xmqocaybg56fqy.us-west-2.es.amazonaws.com/_plugin/kibana/app/kibana

## Looking for Laravel 5.5 support?

1.0 branch supports <= Laravel 5.5. You can find it on our [1.0 branch](https://bitbucket.org/haven_agency/bugcatcher/src/903412900a2fa67d5a8f953879435a0e65439fd0/?at=1.0).


## Installation
Add BugCatcher repo to `composer.json`:
```
"repositories": [{
    "type": "vcs",
    "url": "https://bitbucket.org/haven_agency/bugcatcher.git"
}]
```

Run: 
```bash
$ composer require "haven/bug-catcher:2.0.*"
```

Publish bugcatcher config file: `config/bugcatcher.php`
```bash
$ php artisan vendor:publish --provider="Haven\BugCatcher\ServiceProvider" 
```

Update `config/bugcatcher.php` to your application needs.
Set `name` to the proper application name which is used to differentiate between applications in Kibana. It will typically be the elastic beanstalk environment name. Ex: ln-brisk-front)

Update laravel logging config `config/logging.php`:
```
...
'bugcatcher' => [
    'driver' => 'bugcatcher'
],

'stack' => [
    'driver' => 'stack',
    'channels' => ['bugcatcher', 'single'],
],
...
```


## Configuration
AWS elasticsearch service runs on port 80, so make sure to include ":80" and remove the protocol from ES_HOST.

You'll need to add the following ES environment variables:
```
ES_HOST=SOMEVALUE
ES_KEY=SOMEKEY
ES_SECRET=SOMESECRET
```
See someone for the actual values.

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email dev@havenagency.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.